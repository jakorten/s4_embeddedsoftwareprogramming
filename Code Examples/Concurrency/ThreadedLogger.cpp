/*
Based on the "Modern C++ Challenge" by Bancilla example (p. 141).

Compile with c++ ThreadedLogger.cpp -o ./logger -std=c++17

J.A. Korten / johan.korten@han.nl
March 2020
*/

#include <string>
#include <random>
#include <thread>
#include <future>
#include <iostream>
#include <vector>
 
using namespace std::chrono;
using namespace std;

class logger // implemented as a singleton (design pattern)
{    

public:
    static logger &getInstance() // fundamental method for singleton
    {
        static logger lg;
        return lg;
    }

    logger(logger const &) = delete;
    logger &operator=(logger const &) = delete;

    void log(std::string_view message)
    {
        std::lock_guard<std::mutex> lock(mt);
        std::cout << "LOG: " << message << std::endl;
    }

private:
    std::mutex mt;
    logger() {} // private empty constructor
};

int main()
{
    std::vector<std::thread> modules;
    for (int id = 1; id <= 5; ++id)
    {
        modules.emplace_back([id]() {
            std::random_device rd;
            std::mt19937 mt(rd()); // Uses some pseudo-random generator
            std::uniform_int_distribution<> ud(100, 1000);

            logger::getInstance().log(
                "module " + std::to_string(id) + " started");

            std::this_thread::sleep_for(std::chrono::milliseconds(ud(mt)));
            
            logger::getInstance().log(
                "module " + std::to_string(id) + " finished");
        });
    }
    for (auto &m : modules)
        m.join();
}
