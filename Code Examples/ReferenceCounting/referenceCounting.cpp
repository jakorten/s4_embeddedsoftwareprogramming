class ReferenceCount
{
    int count;
 
    ReferenceCount()
    {
        count = 1; //start at 1 as creation implies at least once reference is being made
    }
 
    void increment()
    {
        count++;
    }
 
    void decrement()
    {
        count--;
        if( count == 0 )
            delete this;
    }
};
 
//any reference counted object simply derives from the above type
class MyType : public ReferenceCount { ... }