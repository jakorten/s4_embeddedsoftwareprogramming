/*
  main.cpp - test program for School / Person (Students / Teachers)
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/

#include <string>
#include <iostream>
#include <vector>

#include "Teacher.h"
#include "Student.h"
#include "School.h"

// Hint: Compile using c++ *.cpp -o school
// Hint: Run using ./school

using namespace std; 

void addTeachers(School *school);
void addStudents(School *school);

int main()
{
    School school;

    addTeachers(&school);
    addStudents(&school);

    cout << endl;

    // persons in the school:
    school.listPeople();

    cout << endl;

    // what are they doing?
    school.whatAreYouDoing();
    cout << endl;
 
    return 0;
}

void addTeachers(School *school) {
    // create some teachers:
    Teacher *t1 = new Teacher("Bob"); 
    Teacher *t2 = new Teacher("Frank");
    Teacher *t3 = new Teacher("Beth");

    // add them to the school:
    school->add(t1);
    school->add(t2);
    school->add(t3);
}

void addStudents(School *school) {
    // create some students
    Student *s1 = new Student("Mary");
    Student *s2 = new Student("Carl");
    Student *s3 = new Student("Eve");

    // add them to the school:
    school->add(s1);
    school->add(s2);
    school->add(s3);
}

