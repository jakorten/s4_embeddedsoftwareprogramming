/*
  School.h - Definition for School
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/

#ifndef SCHOOL
#define SCHOOL

#include <vector>
#include "Person.h"

class School
{
private:
    std::vector<Person*> _people;
    
public:
    void add(Person *person);
    void listPeople() const;
    void whatAreYouDoing() const;
    ~School();
};

#endif