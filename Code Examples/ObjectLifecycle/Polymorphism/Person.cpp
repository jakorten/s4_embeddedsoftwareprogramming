/*
  Person.cpp - Implementation for Abstract class Person 
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences

  Person is a abstract class...
    
    - implements a constructor / destructor... (constructor in .h)
    - a getter method for name
*/

#include <iostream>
#include <string>
#include "Person.h"

using namespace std; 

Person::~Person() {
    // Note: use this to check for possible leaks!
    std::cout << "Virtual person " << _name << " was evaporated into thin air..." << std::endl;
}

const std::string Person::getName() const {
    return _name;
}
