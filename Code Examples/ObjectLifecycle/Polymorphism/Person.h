/*
  Person.h - Definition for Person
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/

#ifndef PERSON
#define PERSON

#include <string>
#include <iostream>

class Person {
    public:
        virtual void whatsUp() = 0;
        const std::string getName() const;
        
        Person(const std::string &name) : _name(name) {
            std::cout << "Person " << name << " was just created..." << std::endl;
        }
        virtual ~ Person();
    private:
        const std::string _name;
};

#endif