/*
  School.cpp - Implementation for School
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/
#include <iostream>
#include <string>
#include <vector>

#include "School.h"
#include "Person.h"

using namespace std;

School::~School() {
    for (auto person : _people) {
        delete person;
    }
}

void School::add(Person *person) {
    _people.push_back(person);
}

void School::listPeople() const {

    /*
    // Only for iterator demonstration purposes:
    // Long version:
    for (auto teacher = _teachers.begin(); teacher != _teachers.end(); teacher++)
    {
        cout << teacher->getName() << endl;
    }
    */

    // Short version:  
    
    for (auto person : _people) {
        cout << person->getName() << endl;
    }
}

void School::whatAreYouDoing() const {
    for (auto person : _people) {
        person->whatsUp();
    }
}
