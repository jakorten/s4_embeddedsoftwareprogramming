/*
  Student.cpp - Implementation for Student
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/ 

#include <iostream>
#include "Student.h"

using namespace std; 

// Note: constructor is borrowed from Person.

void Student::learns() {
    cout << "I am student " << getName() << " and I learn." << endl; 
}

void Student::speaksUp() {
    cout << "Hi teacher!" << endl;
}

void Student::whatsUp() {
    this->learns();
}