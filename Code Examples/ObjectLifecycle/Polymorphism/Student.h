/*
  Student.h - Definition for Student
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/

#ifndef STUDENT
#define STUDENT

#include "Person.h"

class Student : public Person {

public:
    using Person::Person;
    void learns(); 
    void speaksUp(); 
    void whatsUp();
};

#endif