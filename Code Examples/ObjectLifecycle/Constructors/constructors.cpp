#include <iostream>
#include<string>

class Base {
 public:
  int F(int i) { return i; };
};

class Derived : public Base {
 public:
  //using Base::F;
  double F(double d) { return d; };
};

int main() {
    // Note if you forget using using Base::F, this will still work:
    Derived d = Derived();
    std::cout << std::to_string(d.F(10)) << std::endl;
}