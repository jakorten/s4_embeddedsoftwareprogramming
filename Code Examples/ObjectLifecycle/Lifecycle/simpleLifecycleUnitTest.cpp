/*
  simpleLifecycleUnitTest.cpp - Unit Test Demo
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/

#define GENERATE_UNIT_TEST_MAIN
#include "CppUnitTestFramework.hpp"

#include <iostream>

class TestMe {
     
public:
    const int age = 10;

    TestMe() {
        std::cout << "I was created ex nihilo!" << std::endl;
    }

    ~TestMe() {
        std::cout << "My lifecycle ended: TestMe is no more :(" << std::endl;
    }
};

TestMe iLoveTesting;

TEST_CASE(TestMe, Test1) {        

        CHECK_EQUAL(age, 9);

}


