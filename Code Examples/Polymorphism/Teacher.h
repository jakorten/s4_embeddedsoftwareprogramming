/*
  Teacher.h - Definition for Teacher
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/

#ifndef TEACHER
#define TEACHER

#include "Person.h"

class Teacher : public Person {

public:
    using Person::Person;
    void whatsUp();
    void teaches();
    void speaksUp();
private:
    
};

#endif