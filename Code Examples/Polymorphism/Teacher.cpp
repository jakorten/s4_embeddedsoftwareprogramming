/*
  Teacher.cpp - Implementation for Teacher
  Johan Korten
  V1.0 March 2020

  HAN University of Applied Sciences
*/

#include "Teacher.h"
#include <iostream>

using namespace std; 

// Note: constructor is borrowed from Person.

void Teacher::teaches() {
    cout << "I am teacher " << getName() << " and I teach." << endl; 
}

void Teacher::speaksUp() {
    cout << "Hi students!" << endl;
}

void Teacher::whatsUp() {
    this->teaches();
}