/*
    J.A. Korten
    Feb 1, 2022
    V1.1
    Code example OO Points

    Compile with:
    g++ main.cpp Point.cpp -o OOExample

    run with ./OOExample

    Note: if you want to specify compiler version: g++ -std=c++11

*/

#include <stdio.h>
#include <math.h>
#include "Point.h"

int main()
{
    
    Point testPointA = Point(1.0, 1.0);
    Point testPointB = Point(5.0, 5.0);
    
    double distance = testPointA.computeDistance(testPointB);
    printf( "\n Distance: %f\n\n", distance);
    //printf("Hello");
}
