#include "Point.h"
#include <math.h>

Point::Point(double px, double py) {
    x = px;
    y = py;    
}

double Point::computeDistance(const Point& p) const {
    double dx = x - p.x;
    double dy = y - p.y;
    return sqrt(dx*dx + dy*dy);
}