class Point {
    public:
        Point(double px, double py);
        double computeDistance(const Point& p) const;

    private:
        double x;
        double y;
};