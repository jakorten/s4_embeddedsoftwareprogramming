#ifndef IPETCLASS
#define IPETCLASS

using namespace std;

#include "CppInterfaces.h"

DeclareInterface(IPet)
   virtual void gotPetted() = 0;
   virtual string getName() = 0;
EndInterface

#endif