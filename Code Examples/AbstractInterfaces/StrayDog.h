#include "Dog.h"
#include <iostream>

using namespace std;

class StrayDog : public Dog {
    public:
        void barks();
    private:
};

void StrayDog::barks() {
    cout << "Some stray dog: I always bark in fear of the dog catcher!" << endl;
}