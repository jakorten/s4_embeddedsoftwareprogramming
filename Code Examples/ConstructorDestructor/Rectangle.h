#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>

class Rectangle
{
 
public:
   // constructor
   Rectangle(double width, double height) : width_{width}, height_{height} {}   

   // constructor delegation
   Rectangle(); // : Rectangle(width_, height_){}
   
   // overloaded constructor == function overloading
   
   // copy-constructor
   Rectangle(const Rectangle&);

   ~Rectangle(); // destructor


   void print();
   double getWidth() const;
   double getHeight() const;
   void setHeight(double height);
   void setWidth(double height);
   double surfaceArea() const;

   
private:
   // C++11: in-class initialization
   double width_; //{1.0};
   double height_; //{1.0};
};

#endif
