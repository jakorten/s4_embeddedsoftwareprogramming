#include "Rectangle.h"

#include <iostream>

using namespace std;

void println() {
    cout << endl;
}

int main()
{
   Rectangle r1;
   Rectangle r2{1.0, 3.0}; // Uniform initialization: { ..... }

   r1.print();
   r2.print();

   r1 = Rectangle(r2);
   println();

   r1.print();
   r2.print();

   return 0;
}


