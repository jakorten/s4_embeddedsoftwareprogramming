#include "Rectangle.h"

#include <iostream>

using namespace std;


Rectangle::Rectangle()
    : Rectangle(1.0, 1.0)   // Constructor delegation
{}

Rectangle::Rectangle(const Rectangle& rect) { cout << "Copy Constructor Called\n"; } 

Rectangle::~Rectangle() { cout << "Destructor Called\n"; } 

double Rectangle::getWidth() const
{
    return width_;
}

void Rectangle::setWidth(double width)
{
   width_ = width;
}
    
double Rectangle::getHeight() const
{
    return height_;
}

void Rectangle::setHeight(double height)
{
   height_ = height;
}

double Rectangle::surfaceArea() const
{
  return width_ * height_;
}

void Rectangle::print() {
    printf("Width: %f, height: %f\n", width_, height_);
}